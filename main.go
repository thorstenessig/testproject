package main

import (
	"fmt"

	"flamingo.me/testdependency"
)

func main() {
	fmt.Println(testdependency.HelloDependency())
}
